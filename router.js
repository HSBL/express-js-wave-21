const express = require('express')
const router = express.Router()

const timer = (req, res, next) => {
  console.log("TIME : ", Date.now())
  next()
}

router.get('/router', (req, res) => {
  res.send("Hello dari ruter")
})

router.get('/alamat', (req, res) => {
  res.send('Hello dari alamat')
})

router.use(timer)

router.get('/products', (req, res) => {
  res.json(["apple", "Samsung", "One Plus"])
})

router.get('/orders', (req, res) => {
  res.json([{id : 1, paid: false, user_id : 1 }, {id : 2, paid: false, user_id : 2 }])
})

router.get('/sengajaerror', (req, res) => {
  variableBelumDefine // sebuah bug
})

module.exports = router