const express = require("express");
const app = express();
const port = 3000;
const users = [{email : "admin@mail.com", password : "000000"}];

const logger = (req, res, next) => {
  console.log(req.method, req.url);
  next();
};

app.use(logger);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.set("view engine", "ejs");

// third party middleware
const morgan = require("morgan");
app.use(morgan("dev"));

app.get("/", (req, res) => {
  res.send(`Jumlah user yang terdaftar: ${users.length}`);
});

// router endpoint api
const api = require('./routes/api.js')
app.use(api)

app.get("/greet", (req, res) => {
  const name = req.query.name || "Player 1";
  const alamat = req.query.alamat || "Indonesia";
  res.render("greet", { alamat, name });
});

app.get("/register", (req, res) => {
  res.render("register");
});

app.post("/register", (req, res) => {
  const { email, password } = req.body;

  // let isRegistered = false
  
  // users.forEach(user => {
  //   if(user.email === email){
  //     isRegistered = true
  //   }
  // })

  // if(isRegistered){
  //   res.status(400).send('user telah terdaftar')
  // }

  users.push({ email, password });

  res.redirect("/");
});

// router level middleware
const router = require("./router.js");
app.use(router);

// internal server error handler
const internalServerErrHandler = (err, req, res, next) => {
  console.log(err);
  res.status(500).json({ status: "fail", messages: err.message });
};

app.use(internalServerErrHandler);

// 404 handler
app.use((req, res, next) => {
  res.status(404).json({ status: "fail", message: "are you lost" });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
