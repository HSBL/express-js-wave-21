const express = require('express')
const router = express.Router()
let posts = require('../db/posts.json')

router.get('/api/v1/posts/:id', (req, res) => {
  console.log("Tipe Data PARAMS", typeof(+req.params.id))
  const post = posts.find(post => post.id == req.params.id)
  console.log("POST", post)
  res.status(200).json(post)
})

router.get('/api/v1/posts', (req, res) => {
  res.status(200).json(posts)
})

router.post('/api/v1/posts', (req, res) => {
  // tampung dari body request
  const {title, body} = req.body

  // buat id baru
  const id = posts[posts.length -1].id + 1
  // susun object
  const post = {
    id, title, body
  }

  // push array
  posts.push(post)

  // respond code 201
  res.status(201).json(post)
})

router.put('/api/v1/posts/:id', (req, res) => {
  // menemukan postingan dengan id
  let post = posts.find(i => i.id === +req.params.id)
  // mengubah
  const params = {title : req.body.title, body: req.body.body}
  post = {...post, ...params}
  // menyimpan ke dalam database/array
  posts = posts.map(i => i.id === post.id ? post : i)
  // respond ke client
  res.status(200).json(post)
})

router.delete('/api/v1/posts/:id', (req, res) => {
  posts = posts.filter(i => i.id != +req.params.id)
  res.status(200).json({message: `Post dengan id ${req.params.id} berhasil dihapus` })
})

module.exports = router